﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NodeEditorTest.Models
{
    public class Output
    {
        public string name { get; set; }
        public string type { get; set; }
        public int[] links { get; set; }

    }
}
