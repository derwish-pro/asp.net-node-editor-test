﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NodeEditorTest.Models
{
    public class Input
    {
        public string name { get; set; }
        public string type { get; set; }
        public int? link { get; set; }
    }
}
