﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NodeEditorTest.ViewModels.Manage
{
    public class FactorViewModel
    {
        public string Purpose { get; set; }
    }
}
